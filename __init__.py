# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import bom, stock


def register():
    Pool.register(
        bom.BOM,
        bom.BOMInput,
        bom.BOMOutput,
        bom.BOMTree,
        bom.NewVersionStart,
        bom.OpenBOMTreeStart,
        bom.OpenBOMTreeTree,
        bom.Product,
        bom.ProductBom,
        bom.Production,
        bom.Template,
        stock.FillWithProductComponentsAskComponents,
        module='electrans_production_bom_versions', type_='model')

    Pool.register(
        bom.BomExcel,
        bom.OpenBOMTree,
        bom.NewVersion,
        stock.FillWithProductComponents,
        module='electrans_production_bom_versions', type_='wizard')
