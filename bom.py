# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelView, Workflow, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Bool, If, Not
from trytond.transaction import Transaction
import datetime
from trytond.exceptions import UserError, UserWarning
from trytond.i18n import gettext

__all__ = ['BOM', 'Production', 'Product', 'BOMTree', 'OpenBOMTree', 'OpenBOMTreeTree', 'BOMTree', 'ProductBom',
           'Template', 'BOMInput', 'BOMOutput', 'NewVersionStart']


class BOM(metaclass=PoolMeta):
    __name__ = 'production.bom'

    bom_versions = fields.Function(fields.One2Many('production.bom', 'master_bom', 'Versions',), 'get_bom_versions')

    active_version = fields.Function(fields.Boolean('Active'), 'get_active_version', searcher='search_active_version')
    future_version = fields.Function(fields.Boolean('Future Version'), 'get_future_version', searcher='search_future_version')
    # OQA 22/05/2018: This field is used to eval when a register is being created, because it have to be editable.
    created_object = fields.Function(fields.Boolean('Created object'), 'get_created_object')
    used_in_productions = fields.Function(fields.Char('Used in productions'), 'get_used_in_productions')
    is_master_bom = fields.Function(fields.Boolean('Is master bom'), 'get_is_master_bom', searcher='search_is_master_bom')
    # This field is used to put the fields state to readonly when return True.
    is_readonly = fields.Function(fields.Boolean('Readonly'), 'get_is_readonly')
    # If True, don't allow to fabricate productions with a previous bom
    blocking = fields.Boolean('Blocking')
    blocked = fields.Function(fields.Boolean('Blocked list'), 'get_blocked_list', searcher='search_blocked_list')

    @classmethod
    def __setup__(cls):
        """
        Iterate all fields of the model and assign same readonly evaluation to all of them
        """
        super(BOM, cls).__setup__()
        cls._order = [('name', 'ASC'), ('version', 'ASC')]

        _fields = dir(cls)
        cls.end_date.states['readonly'] = True

        noreadonly_fields = ['end_date', 'active', 'name']
        for field in noreadonly_fields:
            _fields.remove(field)

        for fname in _fields:
            field = getattr(cls, fname)
            if hasattr(field, 'states'):
                if not isinstance(field, fields.Function) or isinstance(field, fields.MultiValue):
                    field.states = {
                        'readonly': Eval('is_readonly', False)
                    }

    @classmethod
    def delete(cls, boms):
        for bom in boms:
            if cls.get_last_version(bom.master_bom).id != bom.id or bom.used_in_productions:
                raise UserError(gettext('electrans_production_bom_versions.delete_cancel'))
            else:
                master_bom = bom.master_bom
                version = bom.version-1
                super(BOM, cls).delete([bom])
                cls.write(cls.search([('master_bom', '=', master_bom), ('version', '=', version)]),
                          {'end_date': None})

    @classmethod
    def write(cls, *args):
        actions = iter(args)
        for boms, vals in zip(actions, actions):
            # OQA 26/04/18: if start  date is changed, it will change the previous bom version end date.
            if 'start_date' in vals:
                for bom in cls.browse(boms):
                    if bom.master_bom and bom.version:
                        with Transaction().set_context(show_versions=True):
                            previous_bom = cls.search([
                                ('master_bom', '=', bom.master_bom),
                                ('version', '=', bom.version - 1)
                            ], limit=1)
                            if previous_bom:
                                super(BOM, cls).write(previous_bom,
                                                      {'end_date': vals['start_date'] - datetime.timedelta(days=1)})
            # OQA 26/04/18: if end date is changed, it will change the next bom version start date.
            if 'end_date' in vals:
                for bom in cls.browse(boms):
                    if bom.master_bom and bom.version:
                        with Transaction().set_context(show_versions=True):
                            next_bom = cls.search([
                                ('master_bom', '=', bom.master_bom),
                                ('version', '=', bom.version + 1)
                            ], limit=1)
                        if next_bom:
                            super(BOM, cls).write(next_bom,
                                                  {'start_date': vals['end_date'] + datetime.timedelta(days=1)})
            # There is used the context to don't make an infinite recursion.
            if 'active' in vals and not Transaction().context.get('modify_active'):
                for bom in cls.browse(boms):
                    with Transaction().set_context(show_versions=True):
                        boms = cls.search([
                            ('master_bom', '=', bom.master_bom),
                            ('active', 'in', (True, False))
                        ])
                        with Transaction().set_context(modify_active=True):
                            cls.write(boms, {'active': vals['active']})
            # There is used the context to don't make an infinite recursion.
            if 'name' in vals and not Transaction().context.get('modify_active'):
                for bom in cls.browse(boms):
                    versions = bom.bom_versions
                    with Transaction().set_context(modify_active=True):
                        cls.write([version for version in versions], {'name': vals['name']})

        return super(BOM, cls).write(*args)

    @classmethod
    def copy(cls, boms, default=None):
        boms = super(BOM, cls).copy(boms, default=default)
        if not Transaction().context.get('new_version', False):
            for bom in boms:
                bom.name = 'Copia de ' + bom.name
            cls.save(boms)
        return boms

    @classmethod
    def search(cls, args, offset=0, limit=None, order=None, count=False,
            query=False):
        transaction = Transaction()
        context = transaction.context
        if context.get('active_bom', False):
            args.append(('active', 'in', (True, False)))
        if context.get('active_version', False):
            args.append(('active_version', '=', True))
        with Transaction().set_context(show_versions=True):
            return super(BOM, cls).search(args, offset=offset, limit=limit,
                order=order, count=count, query=query)

    def get_rec_name(self, name):
        rec_name = super(BOM, self).get_rec_name(name)
        if self.is_master_bom:
            return rec_name

        return rec_name + " - v." + str(self.version) + " (Vigente)" if self.active_version and self.active \
            else rec_name + " - v." + str(self.version)

    def get_bom_versions(self, name):
        return [bom.id for bom in self.search([('master_bom', '=', self.master_bom)])] if self.master_bom else []

    @classmethod
    def search_active_version(cls, name, clause):
        today = datetime.date.today()
        if clause[2]:
            return [('start_date', '<=', today), ['OR', ('end_date', '=', None), ('end_date', '>=', today)]]
        else:
            with Transaction().set_context(show_versions=True):
                return ['OR', [('start_date', '<', today), ('end_date', '<', today)],
                        [('start_date', '>', today)]]

    @classmethod
    def get_last_version(cls, master_bom):
        with Transaction().set_context(active_bom=True):
            return super(BOM, cls).get_last_version(master_bom)

    def get_active_version(self, name):
        today = datetime.date.today()
        if not self.end_date:
            if self.start_date <= today:
                return True
        elif self.start_date <= today <= self.end_date:
            return True

    @classmethod
    def search_future_version(cls, name, clause):
        today = datetime.date.today()
        if clause[2]:
            return [('start_date', '>', today)]
        else:
            with Transaction().set_context(show_versions=True):
                return [('start_date', '<', today)]

    def get_future_version(self, name):
        today = datetime.date.today()
        return True if self.start_date > today else False

    def get_created_object(self, name):
        return True

    @classmethod
    def get_used_in_productions(cls, boms, name):
        Production = Pool().get('production')
        ids = list(map(int, boms))
        res = dict.fromkeys(ids, None)
        productions = Production.search([('bom', 'in', ids)])
        for b in boms:
            count = sum(1 for p in productions if p.bom == b)
            res.update({b.id: str(count) if count else None})
        return res

    @staticmethod
    def default_start_date():
        return datetime.date.today() + datetime.timedelta(days=1)

    @classmethod
    def search_is_master_bom(cls, name, clause):
        cursor = Transaction().connection.cursor()
        cursor.execute('SELECT id FROM production_bom where id = master_bom')
        return [('id', 'in', [x[0] for x in cursor.fetchall()])]

    def get_is_master_bom(self, name):
        return True if self.master_bom and self.id == self.master_bom.id else False

    def compute_factor(self, product, quantity, uom):
        """
        OQA: 20/07/2018
        return 0.0 when factor is null to prevent an error.
        This case can only happen when the bom output is different from the
        product related with the bom
        """
        factor = super(BOM, self).compute_factor(product, quantity, uom)
        if not factor:
            raise UserError(gettext(
                'electrans_production_bom_versions.product_do_not_match_bom',
                product=product.rec_name))
        return factor

    def get_active_bom(self, date=None):
        domain = [('master_bom', '=', self.master_bom.id), ('active', '=', True)]
        if date:
            domain.extend([('start_date', '<=', date),
                          ['OR', ('end_date', '>=', date), ('end_date', '=', None)]])
        else:
            domain.append(('active_version', '=', True))
        active_bom = Pool().get('production.bom').search(domain)
        return active_bom[0] if active_bom else None

    def get_is_readonly(self, name):
        if not self.created_object:
            return False
        if self.used_in_productions:
            return True
        return not (getattr(self, 'future_version', True) or getattr(self, 'active_version', True))

    def get_blocked_list(self, name):
        Bom = Pool().get('production.bom')
        Date = Pool().get('ir.date')
        return bool(Bom.search([('master_bom', '=', self.master_bom), ('version', '>', self.version),
                                ('blocking', '=', True), ('start_date', '<=', Date.today())]))

    @classmethod
    def search_blocked_list(cls, name, clause):
        for bom in cls.search([('blocking', '=', True)]):
            childs_blocked = cls.search([('master_bom', '=', bom.master_bom), ('version', '<', bom.version)])
        blocked = [bom.id for bom in childs_blocked]
        if clause[2]:
            return [('id', 'in', blocked)]
        else:
            return [('id', 'not in', blocked)]


class NewVersionStart(metaclass=PoolMeta):
    __name__ = 'production.bom.new.version.start'

    blocking = fields.Boolean('Blocking')


class NewVersion(metaclass=PoolMeta):
    __name__ = 'production.bom.new.version'

    def do_create_(self, action, data=None):
        pool = Pool()
        Production = pool.get('production')
        ProductionBom = pool.get('production.bom')
        Warning = pool.get('res.user.warning')
        bom = ProductionBom.browse(Transaction().context.get('active_ids'))

        blocking = self.start.blocking
        versions = bom[0].bom_versions
        if blocking:
            if Production.search([('state', 'in', ['waiting', 'assigned', 'running']),
                                  ('bom', 'in', [version.id for version in versions])], limit=1):
                key = '%s.create' % bom[0].id
                if Warning.check(key):
                    raise UserWarning(key, 'electrans_production_bom_versions.used_in_productions')

        ProductionBom.write([ProductionBom(bom_id) for bom_id in data['res_id']], {'blocking': blocking})
        return action, data


class Production(metaclass=PoolMeta):
    __name__ = 'production'

    active_version = fields.Function(fields.Boolean('Active version'), 'get_active_version')

    @classmethod
    def __setup__(cls):
        super(Production, cls).__setup__()
        cls.bom.search_context = {'active_version': True}

    @classmethod
    def view_attributes(cls):
        # Highlight in red the productions that do not have an active bill of material
        # and their state is neither cancelled nor finished
        return super().view_attributes() + [
            ('/tree',
             'visual', If(
                Not(Eval('state').in_(['cancelled', 'done'])) &
                Bool(Eval('bom')) & Not(Eval('active_version')), 'danger', ''),
             ['state', 'active_version', 'bom'])]

    def get_active_version(self, name):
        return self.bom and self.bom.get_active_version(name)

    def check_bom(self, state):
        Warning = Pool().get('res.user.warning')
        pool = Pool()
        Bom = pool.get('product.product-production.bom')
        if self.state == state:
            bom = self.bom
            if bom and not bom.active_version and bom.blocked:
                raise UserError(gettext('electrans_production_bom_versions.incorrect_bom'))
            else:
                relations = []
                if bom and self.product:
                    relations = Bom.search([('product', '=', self.product.id),
                                            ('bom', '=', bom.master_bom)])
                if bom and state == 'draft' and not relations:
                    raise UserError(gettext('electrans_production_bom_versions.bom_not_linked_to_product'))
                elif bom and not bom.active_version:
                    key = '%s.create' % self.id
                    if Warning.check(key):
                        raise UserWarning(key, gettext('electrans_production_bom_versions.not_active_version'))

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, productions):
        for production in productions:
            production.check_bom('request')
        super(Production, cls).draft(productions)

    @classmethod
    @ModelView.button
    @Workflow.transition('waiting')
    def wait(cls, productions):
        for production in productions:
            production.check_bom('draft')
        super(Production, cls).wait(productions)

    @classmethod
    @ModelView.button
    @Workflow.transition('assigned')
    def assign(cls, productions):
        for production in productions:
            production.check_bom('waiting')
        super(Production, cls).assign(productions)

    @classmethod
    @ModelView.button
    @Workflow.transition('running')
    def run(cls, productions):
        for production in productions:
            production.check_bom('assigned')
        super(Production, cls).run(productions)


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    # OQA 28/05/2018: override get_output_products function without call super. This function is used to create
    # production.bom.reverse_tree.open wizard.
    @classmethod
    def get_output_products(cls, products, name):
        pool = Pool()
        Input = pool.get('production.bom.input')
        inputs = Input.search([
            ('product', 'in', [x.id for x in products]), ('bom.active_version', '=', True), ('bom.active', '=', True)
            ])
        output_products = {}
        for product in products:
            output_products[product.id] = []
        for input_ in inputs:
            for product in input_.bom.output_products:
                output_products[input_.product.id].append(product.id)
        return output_products


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    def get_active_boms(self, date=None):
        boms = []
        for bom in self.boms:
            bom2 = bom.bom.get_active_bom(date) if bom.bom else None
            if bom2:
                boms.append(bom2.id)
        return boms


class BOMTree(metaclass=PoolMeta):
    __name__ = 'production.bom.tree'

    bom = fields.Many2One('production.bom', 'Bom')
    subcontract_product = fields.Many2One('product.product', 'Subcontract Product')
    # document = fields.Many2One('production.document.edition', 'Document')
    boms = fields.One2Many('production.bom', None, 'Bom')
    prevent_productions_without_substitute = fields.Boolean("Prevent in production without substitute")

    @classmethod
    def view_attributes(cls):
        """
            Color if product don't have substituted but are deprecated
        """
        return super().view_attributes() + [
            ('/tree', 'visual',
             If(Bool(Eval('prevent_productions_without_substitute')), 'danger', ''))]

    # OQA 12/06/18: This function do not call super, but the code is practically the same.
    # I did that way, because i didn't knew how to show active versions calling super function.
    @classmethod
    def tree(cls, product, quantity, uom, bom=None):
        Input = Pool().get('production.bom.input')
        Product = Pool().get('product.product')
        result = []
        if not bom:
            return result
        factor = bom.compute_factor(product, quantity, uom)
        if Transaction().context.get('no_stock'):
            no_stock = True
            input_stock = {}
            output_stock = {}
        else:
            no_stock = None
            input_stock = cls.get_input_output_product([input.product for input in bom.inputs], 'input_stock')
            output_stock = cls.get_input_output_product([input.product for input in bom.inputs], 'output_stock')

        tree_date = Transaction().context.get('tree_date', None)
        for input_ in bom.inputs:
            bom = []
            product_input = input_.product
            prevent_without_substitute = False
            if Transaction().context.get('get_substitutes'):
                if product_input.template.deprecated and product_input.template.prevent_in_production and product_input.template.substitute:
                    product_input = Product(product_input.get_substitute())
                prevent_without_substitute = product_input.template.prevent_in_production
            for vbom in product_input.boms:
                if vbom.bom and vbom.bom.master_bom:
                    active_bom = vbom.bom.get_active_bom(tree_date)
                    if active_bom:
                        bom = active_bom
                        break
            quantity = Input.compute_quantity(input_, factor)
            document = getattr(bom, 'production_doc_edition', None)
            childs = cls.tree(product_input, quantity, input_.uom, bom)
            boms = product_input.template.get_active_boms(tree_date)
            values = {
                'factor': factor,
                'product': product_input.id,
                'product.': {
                    'rec_name': input_.product.rec_name,
                    },
                'quantity': quantity,
                'uom': input_.uom.id,
                'uom.': {
                    'rec_name': input_.uom.rec_name
                    },
                'unit_digits': input_.uom.digits,
                'childs': childs,
                'bom': bom.id if bom else None,
                'bom.': {
                    'rec_name':  bom.rec_name if bom else '',
                    },
                'boms': boms,
                'document': document.id if document else None,
                'document.': {
                    'rec_name': document.rec_name if document else '',
                    },
                'subcontract_product': bom.subcontract_product.id if bom and bom.subcontract_product else None,
                'subcontract_product.': {
                    'rec_name':  bom.subcontract_product.rec_name if bom and bom.subcontract_product else '',
                    },
                'input_stock': input_stock.get(product_input.id, None),
                'output_stock': output_stock.get(product_input.id, None),
                'current_stock': product_input.quantity if not no_stock else None,
                'prevent_productions_without_substitute': prevent_without_substitute
            }
            result.append(values)
        return result


class OpenBOMTree(metaclass=PoolMeta):
    __name__ = 'production.bom.tree.open'

    def default_start(self, fields):
        defaults = super(OpenBOMTree, self).default_start(fields)
        Template = Pool().get('product.template')
        Product = Pool().get('product.product')
        active_id = Transaction().context.get('active_id')
        Location = Pool().get('stock.location')
        product_id = active_id if Transaction().context.get('active_model') == 'product.product' \
            else Template(active_id).products[0] if Template(active_id).unique_variant else None
        product = Product(product_id)
        for bom in product.boms:
            if bom.bom and bom.bom.master_bom:
                active_bom = Pool().get('production.bom').search([('master_bom', '=', bom.bom.master_bom.id),
                                                                  ('active_version', '=', True), ('active', '=', True)])
                if active_bom:
                    defaults['bom'] = bom.id
        warehouse = Location.search([('code', '=', 'WH')])
        defaults['warehouse'] = warehouse[0].id if warehouse else None
        defaults['no_stock_count'] = True
        return defaults

    def default_tree(self, fields):
        tree_date = self.start.date
        with Transaction().set_context(tree_date=tree_date, no_stock=self.start.no_stock_count):
            return super(OpenBOMTree, self).default_tree(fields)


class OpenBOMTreeTree(metaclass=PoolMeta):
    __name__ = 'production.bom.tree.open.tree'

    bom_tree = fields.One2Many('production.bom.tree', None, 'BOM Tree')

    @classmethod
    def tree(cls, bom, product, quantity, uom):
        tree_date = Transaction().context.get('tree_date', None)
        active_bom = bom.get_active_bom(tree_date)
        bom = active_bom if active_bom else bom

        if Transaction().context.get('no_stock'):
            Tree = Pool().get('production.bom.tree')
            childs = Tree.tree(product, quantity, uom, bom=bom)
            tree = {'bom_tree': [{
                    'product': product.id,
                    'product.rec_name': product.rec_name,
                    'quantity': quantity,
                    'uom': uom.id,
                    'uom.rec_name': uom.rec_name,
                    'unit_digits': uom.digits,
                    'childs': childs,
            }]}
        else:
            tree = super(OpenBOMTreeTree, cls).tree(bom if bom else None,  product, quantity, uom)
        tree = tree['bom_tree'][0]
        tree['bom'] = bom.id if bom else None
        tree['document'] = (
            getattr(bom, 'production_doc_edition').id if getattr(bom, 'production_doc_edition') else None)
        tree_date = Transaction().context.get('tree_date', None)
        tree['boms'] = product.template.get_active_boms(tree_date)
        tree['subcontract_product'] = bom.subcontract_product.id if bom and bom.subcontract_product else None
        return {
            'bom_tree': [tree],
        }


class ProductBom(metaclass=PoolMeta):
    __name__ = 'product.product-production.bom'

    active_version = fields.Function(fields.Many2One('production.bom', 'Active version'), 'get_active_version')

    def get_active_version(self, name):
        active = self.bom.get_active_bom()
        return active.id if active else None

    @classmethod
    def __setup__(cls):
        super(ProductBom, cls).__setup__()
        cls.bom.domain += [('is_master_bom', '=', True)]


class BOMInput(metaclass=PoolMeta):
    __name__ = 'production.bom.input'

    is_bom_readonly = fields.Function(fields.Boolean('Readonly'), 'get_is_bom_readonly')

    @fields.depends('bom')
    def get_is_bom_readonly(self, name):
        return self.bom.is_readonly

    @classmethod
    def __setup__(cls):
        super(BOMInput, cls).__setup__()
        for fname in dir(cls):
            field = getattr(cls, fname)
            if hasattr(field, 'states'):
                if not isinstance(field, fields.Function) or isinstance(field, fields.MultiValue):
                    field.states = {
                        'readonly': Eval('is_bom_readonly', False)
                    }


class BOMOutput(metaclass=PoolMeta):
    __name__ = 'production.bom.output'

    is_bom_readonly = fields.Function(fields.Boolean('Readonly'), 'get_is_bom_readonly')

    @fields.depends('bom')
    def get_is_bom_readonly(self, name):
        return self.bom.is_readonly

    @classmethod
    def __setup__(cls):
        super(BOMOutput, cls).__setup__()
        for fname in dir(cls):
            field = getattr(cls, fname)
            if hasattr(field, 'states'):
                if not isinstance(field, fields.Function) or isinstance(field, fields.MultiValue):
                    field.states = {
                        'readonly': Eval('is_bom_readonly', False)
                    }


class OpenBOMTreeStart(metaclass=PoolMeta):
    __name__ = 'production.bom.tree.open.start'

    no_stock_count = fields.Boolean('No Stock')


class BomExcel(metaclass=PoolMeta):
    __name__ = 'production.bom.excel'

    def do_print_(self, action):
        action, data = super(BomExcel, self).do_print_(action)
        data['no_stock_count'] = self.start.no_stock_count
        return action, data
