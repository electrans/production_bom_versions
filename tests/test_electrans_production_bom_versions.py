#!/usr/bin/env python
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import trytond.tests.test_tryton
from trytond.modules.company.tests import create_company, set_company
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.exceptions import UserError, UserWarning
import datetime
from trytond.modules.electrans_tools.tests.test_tools import (
    create_fiscalyear_and_chart, get_accounts, create_production,
    create_account_category, _create_bom, create_product)


class TestCase(ModuleTestCase):
    'Test module'
    module = 'electrans_production_bom_versions'

    @with_transaction()
    def test_fill_with_product_components_wizard(self):
        "Test fill with product components wizard"
        pool = Pool()
        InternalShipment = pool.get('stock.shipment.internal')
        Location = pool.get('stock.location')
        ProductUom = pool.get('product.uom')
        Tax = pool.get('account.tax')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        WizardFillWithComponents = pool.get(
            'stock.shipment.internal.fill_with_product_components',
            type='wizard')
        company = create_company()
        with set_company(company):
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category = create_account_category(tax, expense, revenue)
            # Create product
            template, product = create_product('Product', account_category)
            template.purchasable = True
            template.producible = True
            template.save()
            # Create bom
            template, input_component = create_product(
                'input_component', account_category, consumable=False)
            template.purchasable = True
            template.producible = True
            template.save()
            bom = _create_bom(product, input_component, unit)
            bom.start_date = datetime.date.today()
            bom.active_version = True
            bom.save()
            # Create stock locations
            storage, = Location.search([('code', '=', 'STO')])
            storage2, = Location.create([{
                'name': 'Storage 2',
                'code': 'STO2',
                'type': 'storage',
                'parent': storage.id,
            }])
            # Create internal shipments
            internal_shipment = InternalShipment()
            internal_shipment.related_to = "Testing"
            internal_shipment.company = company.id
            internal_shipment.from_location = storage.id
            internal_shipment.to_location = storage2.id
            internal_shipment.save()
            internal_shipment_2 = InternalShipment()
            internal_shipment_2.related_to = "Testing_User_Warning"
            internal_shipment_2.company = company.id
            internal_shipment_2.from_location = storage.id
            internal_shipment_2.to_location = storage2.id
            internal_shipment_2.save()
            # Wizard test
            session_id, _, _ = WizardFillWithComponents.create()
            wizard_fill_with_components = WizardFillWithComponents(session_id)
            wizard_fill_with_components.record = internal_shipment
            # Saves the number of moves before executing the wizard
            len_initial_moves = len(internal_shipment.moves)
            with Transaction().set_context(active_ids=[internal_shipment],
                                           active_model='stock.shipment.internal'):
                internal_shipment.state = 'assigned'
                internal_shipment.save()
                self.assertRaises(UserError,
                                  wizard_fill_with_components.transition_start)
                internal_shipment.state = 'shipped'
                internal_shipment.save()
                self.assertRaises(UserError,
                                  wizard_fill_with_components.transition_start)
                internal_shipment.state = 'done'
                internal_shipment.save()
                self.assertRaises(UserError,
                                  wizard_fill_with_components.transition_start)
                internal_shipment.state = 'cancelled'
                internal_shipment.save()
                self.assertRaises(UserError,
                                  wizard_fill_with_components.transition_start)
                internal_shipment.state = 'draft'
                internal_shipment.save()
                wizard_fill_with_components.transition_start()
                wizard_fill_with_components.ask_components.product = product
                wizard_fill_with_components.ask_components.bom = bom
                wizard_fill_with_components.ask_components.quantity = 1
                wizard_fill_with_components.ask_components.uom = unit
                wizard_fill_with_components._execute('create_')

                # Adding a 2nd record to check if wizard raises the warning
                wizard_fill_with_components.records = [internal_shipment,
                                                       internal_shipment_2]
                self.assertRaises(UserWarning,
                                  wizard_fill_with_components.transition_start)
            # Saves the number of moves after executing the wizard
            len_final_moves = len(internal_shipment.moves)
            # Saves the number of moves which should be added by the wizard
            len_inputs = len(
                wizard_fill_with_components.ask_components.bom.inputs)
            # Saves the number of moves added by the wizard
            len_added_moves = len_final_moves - len_initial_moves
            # Check if the wizard added the correct amount of moves
            self.assertTrue(len_added_moves == len_inputs)

    @with_transaction()
    def test_production_bom_domain(self):
        "Test Production Bom Domain"
        pool = Pool()
        Location = pool.get('stock.location')
        Tax = pool.get('account.tax')
        ProductUom = pool.get('product.uom')
        Party = pool.get('party.party')
        Employee = pool.get('company.employee')
        Production = pool.get('production')
        ProductBom = pool.get('product.product-production.bom')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        company = create_company()
        with set_company(company):
            # Create party
            party = Party(name='Supplier 1')
            # Create employee
            employee = Employee(party=party)
            employee.save()
            # Create all necessary locations
            production_loc, = Location.search([('code', '=', 'PROD')])
            storage_location, = Location.search([('code', '=', 'STO')])
            warehouse, = Location.search([('code', '=', 'WH')])
            warehouse.production_location = production_loc
            warehouse.storage_location = storage_location
            warehouse.save()
            location_sto2 = Location(name='STO 2',
                                     type='storage', code='STO2')
            storage_location.provisioning_location_edit = True
            storage_location.provisioning_location_used = location_sto2
            storage_location.save()
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category_goods = create_account_category(
                tax, expense, revenue)
            # Create products and bom
            template1, product_1 = create_product(
                'Component 1', account_category_goods, consumable=False)
            template1.purchasable = True
            template1.producible = True
            template1.save()
            template2, product_2 = create_product(
                'Component 2', account_category_goods, consumable=True)
            template2.purchasable = True
            template2.producible = True
            template2.save()
            bom = _create_bom(product_1, product_2, unit)
            bom.save()
            # To make bom active
            bom.start_date = bom.start_date - datetime.timedelta(days=2)
            bom.save()
            # Check that if bom has not been created from product directly, bom can not be selected for the production
            with self.assertRaises(UserError):
                production = create_production(3, product_1, unit, warehouse, production_loc, company, bom)
                Production.draft([production])
                Production.wait([production])
            # Bom correctly linked with product but not active,
            bom.start_date = bom.start_date + datetime.timedelta(days=2)
            bom.save()
            with self.assertRaises(UserWarning):
                ProductBom = pool.get('product.product-production.bom')
                product_bom = ProductBom()
                product_bom.bom = bom
                product_bom.product = product_1
                product_bom.save()
                Production.draft([production])
                Production.wait([production])
            # Bom active,
            bom.start_date = bom.start_date - datetime.timedelta(days=2)
            bom.save()
            # Bom now will be active but not correctly linked with product
            Production.draft([production])
            Production.wait([production])


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(TestCase))
    return suite
