# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal

from trytond.exceptions import UserError, UserWarning
from trytond.model import ModelView, fields
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.i18n import gettext
from trytond.wizard import (
    Wizard, StateTransition, StateView, Button, StateAction)

__all__ = ['FillWithProductComponentsAskComponents',
           'FillWithProductComponents']


class FillWithProductComponentsAskComponents(ModelView):
    'Fill With Product Components Ask Components'
    __name__ = 'stock.shipment.internal.fill_with_product_components.ask_components'
    product = fields.Many2One('product.product', 'Product', required=True,
        domain=[('producible', '=', True)])
    quantity = fields.Float('Quantity', required=True,
        digits=(16, Eval('unit_digits', 2)), depends=['unit_digits'])
    unit_digits = fields.Integer('Unit Digits', readonly=True)
    category = fields.Many2One('product.uom.category', 'Category',
        readonly=True)
    bom = fields.Many2One('production.bom', 'BOM',
        required=True,
        domain=[
            ('active_version', '=', True),
            ('output_products', '=', Eval('product', 0))
        ],
        depends=['product'])
    uom = fields.Many2One('product.uom', 'Uom', required=True,
                          domain=[
                              ('category', '=', Eval('category'))
                          ],
                          depends=['category'])

    @fields.depends('product')
    def on_change_product(self):
        pool = Pool()
        Bom = pool.get('production.bom')
        boms_list = []
        if self.product:
            boms_list = Bom.search([
                ('active_version', '=', True),
                ('output_products', '=', self.product.id)
            ])
            self.quantity = Decimal('1')
            self.category = self.product.default_uom.category
            self.uom = self.product.template.default_uom
        if len(boms_list) == 1:
            self.bom = boms_list[0]

    @fields.depends('uom')
    def on_change_with_unit_digits(self):
        if self.uom:
            return self.uom.digits
        return 2


class FillWithProductComponents(Wizard):
    'Fill With Product Components'
    __name__ = 'stock.shipment.internal.fill_with_product_components'

    start = StateTransition()
    ask_components = StateView(
        'stock.shipment.internal.fill_with_product_components.ask_components',
        'electrans_production_bom_versions.'
        'shipment_internal_fill_with_product_components_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Fill', 'create_', 'tryton-ok')
        ])

    create_ = StateAction(
        'electrans_production_bom_versions.'
        'act_fill_with_product_components_form'
    )

    def transition_start(self):
        pool = Pool()
        Warning = pool.get('res.user.warning')
        for record in self.records:
            if record.state in ['assigned', 'cancelled', 'done', 'shipped']:
                raise UserError(gettext(
                    'electrans_production_bom_versions.invalid_shipment_state')
                )
        if len(self.records) > 1:
            warning_name = 'self.records.length'
            if Warning.check(warning_name):
                raise UserWarning(warning_name, gettext(
                    'electrans_production_bom_versions.multiple_records_selected'))
        return 'ask_components'

    def do_create_(self, action):
        pool = Pool()
        Move = pool.get('stock.move')
        Input = pool.get('production.bom.input')
        bom = self.ask_components.bom
        input_quantity = self.ask_components.quantity
        product = self.ask_components.product
        uom = self.ask_components.uom

        if not getattr(self.ask_components, 'product', None):
            return 'ask_components'

        factor = bom.compute_factor(product, input_quantity, uom)
        for record in self.records:
            for input_record in bom.inputs:
                # Compute inputs quantity with factor of the provided uom
                quantity = Input.compute_quantity(input_record, factor)
                Move.create([{
                    'product': input_record.product.id,
                    'uom': input_record.uom.id,
                    'quantity': quantity,
                    'from_location': record.from_location.id,
                    'to_location': record.to_location.id,
                    'company': record.company.id,
                    'shipment': record
                }])

        return 'end'
